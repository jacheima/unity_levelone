﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trampoline : MonoBehaviour
{
    Rigidbody rb;
    [SerializeField] private float jumpHeight;

    
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        Rigidbody rbOther = other.gameObject.GetComponent<Rigidbody>();
        Debug.Log("Your In ME!");

        if (rbOther)
        {
            rbOther.AddForce(0, jumpHeight, 0);
            Debug.Log("JUMP!");
        }
        
    }

}
