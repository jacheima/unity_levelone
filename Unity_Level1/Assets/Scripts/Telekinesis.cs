﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Telekinesis : MonoBehaviour
{
    [SerializeField] private GameObject telekinesisPoint;
    [SerializeField] private float pushAmount;

    public bool hasObject;
    public bool isAtPoint;
    private Rigidbody rb;
    

    void Start()
    {
        hasObject = false;
        isAtPoint = false;
    }
    private void Update()
    {
        //Check to see if the player clicked the mouse 0 button
        if (Input.GetButtonDown("Fire1"))
        {
            //if the player clicked & they are holding an object already
            if (hasObject)
            {
                //Throw the object
                ThrowObject();
            }
            else
            {
                //Otherwise, pull the object to you
                PullObject();
            }
        }
    }

    private void ThrowObject()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;

        if (Physics.Raycast(ray, out hit, 100f))
        {
            rb.velocity = (hit.point - rb.gameObject.transform.position) * pushAmount;

            rb.gameObject.transform.parent = null;

            hasObject = false;
        }
    }

    private void PullObject()
    {
        //Raycast from the center of the screen, where the mouse is locked
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

        //holds whatever the raycast hits
        RaycastHit hit;

        //if the raycast hits anything
        if (Physics.Raycast(ray, out hit, 100f))
        {
            //if the raycast hit something with the Telekinesable component
            if (hit.collider.GetComponent<Telekinesable>())
            {
                //Get the rigidbody
                rb = hit.collider.gameObject.GetComponent<Rigidbody>();

                //set the velocity of the object towards the telekinesisPoint
                rb.velocity = telekinesisPoint.transform.position - hit.transform.position;

                //start the floating animaiton
                hit.collider.gameObject.GetComponent<Animator>().SetBool("isFloating", true);

                //parent the object to the telekinesisPoint
                hit.collider.transform.parent = telekinesisPoint.transform;

                hasObject = true;
            }
        }
    }
}
