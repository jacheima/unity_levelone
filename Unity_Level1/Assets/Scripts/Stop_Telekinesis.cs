﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Stop_Telekinesis : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        //if the object that enters is a telekinesisable object
        if (other.gameObject.GetComponent<Telekinesable>())
        {
            //set the velocity of it to zero
            other.GetComponent<Rigidbody>().velocity = new Vector3(0f, 0f, 0f);

            other.gameObject.transform.position = this.gameObject.transform.position;
        }
    }
}
