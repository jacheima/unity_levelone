﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUps : MonoBehaviour
{
    private float rotationSpeed = .2f;
    public string color;

    // Update is called once per frame
    void Update()
    {
        transform.Rotate(Vector3.up, 90 * rotationSpeed * Time.deltaTime);
    }

    void OnTriggerEnter(Collider other)
    {
        Player player = other.gameObject.GetComponent<Player>();

        if (player)
        {
            player.gatheredCrayons++;
            player.GetColor(color);
            Destroy(gameObject);
        }
    }
}
