﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    public int gatheredCrayons = 0;
    private int totalCrayons = 5;

    public Text currentCrayonCount;

    public GameObject trophy;

    public GameObject boxFront;
    public GameObject boxBack;

    public GameObject blueCrayon;
    public GameObject redCrayon;
    public GameObject greenCrayon;
    public GameObject purpleCrayon;
    public GameObject yellowCrayon;

    public bool hasBlue;
    public bool hasRed;
    public bool hasGreen;
    public bool hasPurple;
    public bool hasYellow;
    private bool isMenuOpen;
    private bool isGameDone;


    void Start()
    {
        blueCrayon.SetActive(false);
        redCrayon.SetActive(false);
        greenCrayon.SetActive(false);
        purpleCrayon.SetActive(false);
        yellowCrayon.SetActive(false);

        boxBack.SetActive(false);
        boxFront.SetActive(false);

        currentCrayonCount.enabled = false;

        trophy.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(gatheredCrayons == 5)
        {
            if (isMenuOpen)
            {
                blueCrayon.SetActive(false);
                redCrayon.SetActive(false);
                greenCrayon.SetActive(false);
                purpleCrayon.SetActive(false);
                yellowCrayon.SetActive(false);

                boxBack.SetActive(false);
                boxFront.SetActive(false);

                currentCrayonCount.enabled = false;
            }

            trophy.SetActive(true);
            currentCrayonCount.enabled = true;
            currentCrayonCount.fontSize = 20;
            currentCrayonCount.text = "Press 'Enter' to Quit";
            isGameDone = true;
        }

        if (isGameDone && Input.GetKeyDown(KeyCode.Return))
        {
            Quit();
        }

        if (Input.GetKeyDown(KeyCode.Tab))
        {
            if (isMenuOpen)
            {
                blueCrayon.SetActive(false);
                redCrayon.SetActive(false);
                greenCrayon.SetActive(false);
                purpleCrayon.SetActive(false);
                yellowCrayon.SetActive(false);

                boxBack.SetActive(false);
                boxFront.SetActive(false);

                currentCrayonCount.enabled = false;

                isMenuOpen = false;
            }
            else
            {
                if (hasBlue)
                {
                    blueCrayon.SetActive(true);
                }

                if (hasRed)
                {
                    redCrayon.SetActive(true);
                }

                if (hasGreen)
                {
                    greenCrayon.SetActive(true);
                }

                if (hasPurple)
                {
                    purpleCrayon.SetActive(true);
                }

                if (hasYellow)
                {
                    yellowCrayon.SetActive(true);
                }

                boxBack.SetActive(true);
                boxFront.SetActive(true);
                currentCrayonCount.enabled = true;

                isMenuOpen = true;
            }
            
        }

        if (!isGameDone)
        {
            currentCrayonCount.text = gatheredCrayons + " / " + totalCrayons;
        }
        
    }

    public void GetColor(string color)
    {
        switch (color)
        {
            case "Blue":
                hasBlue = true;
                break;
            case "Red":
                hasRed = true;
                break;
            case "Green":
                hasGreen = true;
                break;
            case "Purple":
                hasPurple = true;
                break;
            case "Yellow":
                hasYellow = true;
                break;
        }
    }

    public void Quit()
    {
        Application.Quit();
    }
}
